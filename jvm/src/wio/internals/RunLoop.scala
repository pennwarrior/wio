package wio.internals

import cats.data.Ior
import cats.effect.IO
import cats.syntax.either._
import cats.syntax.ior._
import shapeless.{::, HList, HNil}

import scala.annotation.tailrec
import scala.collection.concurrent.TrieMap
import scala.collection.mutable.Stack
import scala.reflect.runtime.universe._

import wio._
import wio.tracing.IOEvent
import wio.ops.TaggedInjectHList

import WIO._

private[wio] object RunLoop {
  final case class WIORunFailure(e: Throwable) extends RuntimeException(e)

  private type DepMap      = TrieMap[Any, List[Any]]
  private type Bind        = Any => WIO[_ <: HList, Any, Any]
  private type StackBindOp = (Bind, AnyRef)
  private val injected: DepMap = TrieMap.empty

  sealed trait StackOp

  final case class BindOp(ior: Ior[StackBindOp, StackBindOp]) extends StackOp
  final case class MapOp(ior: Ior[StackBindOp, StackBindOp])  extends StackOp
  final case class TraceOp(trace: AnyRef)                     extends StackOp

  def run(wio: WIO[_ <: HList, Any, Any], ctx: RunLoopContext = null): IO[Either[Any, Any]] = {

    var current        = wio.asInstanceOf[WIO[_ <: HList, Any, Any]]
    lazy val bindStack = new Stack[StackOp]
    lazy val context   = new RunLoopContext(ctx)

    var value: IO[Either[Any, Any]] = null

    def popNextMapOp(): Ior[StackBindOp, StackBindOp] = {
      if (bindStack.isEmpty) null
      else {
        val next = bindStack.pop()
        next match {
          case TraceOp(trace) => { context.pushEvent(trace.asInstanceOf[IOEvent]); popNextMapOp() }
          case MapOp(map)     => map
          case BindOp(_)      => {
            bindStack.push(next)
            null
          }
        }
      }
    }

    def __ur = throw new AssertionError("unreachable")

    def popNextBindOp(): Ior[StackBindOp, StackBindOp] = {
      if (bindStack.isEmpty) null
      else {
        val next = bindStack.pop()
        next match {
          case BindOp(op) => op
          case _          => {
            bindStack.push(next)
            null
          }
        }
      }
    }

    @tailrec
    def processValue(v: IO[Either[Any, Any]]): IO[Either[Any, Any]] = {
      val fn = popNextMapOp()
      if (fn == null && bindStack.isEmpty) v
      else if (fn != null) {
        processValue {
          v.flatMap {
            case r @ Right(a) =>
              if (fn.isLeft) IO.pure(r)
              else {
                val op = fn.getOrElse(__ur)
                context.pushEvent(op._2.asInstanceOf[IOEvent])
                run(
                  {
                    try op._1(a)
                    catch {
                      case e: Throwable => {
                        context.pushErrorEvent(e, op._2.asInstanceOf[IOEvent])
                        throw e
                      }
                    }
                  },
                  context
                )
              }
            case l @ Left(e)  =>
              if (fn.isRight) IO.pure(l)
              else {
                val op = fn.swap.getOrElse(__ur)
                context.pushErrorEvent(WIO.toThrowable(e), op._2.asInstanceOf[IOEvent])
                run(op._1(e), context)
              }
          }
        }
      } else {
        val bindFn = popNextBindOp()
        if (bindFn == null) v
        processValue {
          v.flatMap {
            case r @ Right(a) =>
              if (bindFn.isLeft) IO.pure(r)
              else {
                val op = bindFn.getOrElse(__ur)
                context.pushEvent(op._2.asInstanceOf[IOEvent])
                run(
                  {
                    try op._1(a)
                    catch {
                      case e: Throwable => {
                        context.pushErrorEvent(e, op._2.asInstanceOf[IOEvent])
                        throw e
                      }
                    }
                  },
                  context
                )
              }
            case l @ Left(e)  =>
              if (bindFn.isRight) IO.pure(l)
              else {
                val op = bindFn.swap.getOrElse(__ur)
                context.pushErrorEvent(WIO.toThrowable(e), op._2.asInstanceOf[IOEvent])
                run(op._1(e), context)
              }
          }
        }
      }
    }

    while ({
      current match {
        case Trace(io, trace)         => {
          bindStack.push(TraceOp(trace))
          current = io
        }

        case next @ Map(io, f, trace) => {
          bindStack.push(MapOp((next, trace).rightIor))
          current = io
        }

        case FlatMap(io, f, trace)    => {
          bindStack.push(BindOp((f.asInstanceOf[Bind], trace).rightIor))
          current = io
        }

        case FlatMapD(io, f, trace)   => {
          bindStack.push(BindOp((f.asInstanceOf[Bind], trace).rightIor))
          current = io
        }

        case Delay(a)                 => {
          value = {
            IO.delay {
              try a().asRight
              catch {
                case e: Throwable => { e.asLeft }
              }
            }
          }
          current = null
        }

        case Pure(a)                  => {
          value = IO.pure(a.asRight)
          current = null
        }

        case Error(error)             => {
          value = IO.pure(error.asLeft)
          current = null
        }

        case PullContext(trace)       => {
          value = IO.pure(context.asRight)
          current = null
        }

        case PushContext(io, ctx)     => {
          value = run(io, ctx)
          current = null
        }

        case Require(tag)             => {
          value = readDep(tag, injected).map(_.asRight)
          current = null
        }

        case h: hNilAsAny[_, _, _]    => { current = h.kdep }

        case Suspend(io) => {
          value = IO.defer(run(io(), ctx))
          current = null
        }

        case Ensure(io)  => { current = io }

        case next @ MapError(io, f, trace)           => {
          bindStack.push(MapOp((next, trace).leftIor))
          current = io
        }

        case FlatMapError(io, f, trace)              => {
          bindStack.push(BindOp((f.asInstanceOf[Bind], trace).leftIor))
          current = io
        }

        case FlatMapErrorD(io, f, trace)             => {
          bindStack.push(BindOp((f.asInstanceOf[Bind], trace).leftIor))
          current = io
        }

        case CEIO(io)                                => {
          value = io.attempt
          current = null
        }

        case next @ Fold(io, ef, af, etrace, atrace) => {
          val errFn = { a: Any => Pure(ef(a)) }
          bindStack.push(MapOp(Ior.both((errFn.asInstanceOf[Bind], etrace), (next, atrace))))
          current = io
        }

        case FoldWith(io, ef, af, etrace, atrace)    => {
          bindStack.push(BindOp(Ior.both((ef.asInstanceOf[Bind], etrace), (af.asInstanceOf[Bind], atrace))))
          current = io
        }

        case Inject(io, i, tags, _)                  => {
          insertDep(i, injected, tags)
          current = io <* clearDep(i, injected, tags)
        }

        case InjectPartial(io, i, tags)              => {
          insertDep(i, injected, tags)
          current = io <* clearDep(i, injected, tags)
        }

        case DumpTrace(trace)                        => {
          bindStack.push(TraceOp(trace))
          value = IO.pure(context.trace().asRight)
          current = null
        }
      }

      if (value != null) {
        value = processValue(value)
      }

      (value == null && current != null)
    }) ()

    if (value == null) throw new WIORunFailure(new NullPointerException("WIORunLoop computed no value"))

    value
  }

  private def clearDep[W0 <: HList](dep: W0, dm: DepMap, tags: TaggedInjectHList[W0]) =
    PIO {
      import scala.collection.immutable.{:: => |:}

      @tailrec
      def loop(a: HList, b: List[String]): Unit =
        (a, b) match {
          case (HNil, Nil)        => ()
          case (h :: t, th |: tt) => {
            dm.updateWith(th)(_.map(l => l.tail))
            loop(t, tt)
          }
          case _                  => assert(false, s"HLists do not match $dep <<>> ${tags.types}")
        }

      loop(dep, tags.types)
    }

  private def readDep[W <: AnyRef](tag: TypeTag[W], deps: DepMap): IO[W] = {
    IO.fromOption(deps.get(TaggedInjectHList.stringifyType(tag.tpe)).flatMap(_.headOption.map(_.asInstanceOf[W]))) {
      new AssertionError(s"Unreachable.  Tag:${tag.tpe}:, HLIST:${deps}:  val:${deps.get(tag.tpe)}")
    }
  }

  private def insertDep[W0 <: HList](dep: W0, dm: DepMap, tags: TaggedInjectHList[W0]): Unit = {
    import scala.collection.immutable.{:: => |:}

    @tailrec
    def loop(a: HList, b: List[String]): Unit =
      (a, b) match {
        case (HNil, Nil)        => ()
        case (h :: t, th |: tt) => {
          dm.updateWith(th)(_.map(l => h :: l).orElse(Some(h :: Nil)))
          loop(t, tt)
        }
        case _                  => assert(false, s"HLists do not match $dep <<>> ${tags.types}")
      }

    loop(dep, tags.types)
  }
}
