/*
 * == Adapted from Cats Effect ==
 * This file is nearly identical to `cats.effect.internals.IOContext` with the exception
 * of the use of `WIOTraceImpl` (instead of `IOTrace`) and `ArrayDeque` (instead of `RingBuffer`)
 * 
 * Original copyright listed below
 * ==============================
 * 
 * Copyright (c) 2017-2021 The Typelevel Cats-effect Project Developers
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package wio.internals

import wio.tracing.IOEvent

import scala.collection.mutable.ArrayDeque

import wio.tracing.{WIOTrace, WIOTraceImpl}
import scala.reflect.NameTransformer

private[wio] class RunLoopContext(initEvents: RunLoopContext) {
  private val events: ArrayDeque[IOEvent] = new ArrayDeque(RunLoopContext.LogSize)  
  private var captured: Int = 0
  private var omitted: Int = 0

  if (initEvents != null) {
    events.addAll(initEvents.events)
    captured += initEvents.captured
    omitted += initEvents.omitted
  }

  def pushEvent(fr: IOEvent): Unit = {
    captured += 1
    events.prepend(fr)
    if (events.size >= RunLoopContext.LogSize) {
      omitted += 1
      events.removeLast()
    }
  }

  def pushErrorEvent(e: Throwable, fr: IOEvent): Unit = {
    if (fr != null) pushEvent(fr)
    RunLoopContext.augmentException(e, this)
  }

  def trace(): WIOTrace =
    WIOTraceImpl(events.toList, captured, omitted)

  def getStackTraces(): List[IOEvent.StackTrace] =
    events.toList.collect { case ev: IOEvent.StackTrace => ev }

}

object RunLoopContext {
  private val LogSize = 1024

  private[this] val runLoopFilter = List(
    "wio.",
    "cats.effect.",
    "scala.runtime."
  )

  /**
    * If stack tracing and contextual exceptions are enabled, this
    * function will rewrite the stack trace of a captured exception
    * to include the async stack trace.
    * 
    * @note Copied from `cats.effect.internals.IORunLoop`
    */
  private def augmentException(ex: Throwable, ctx: RunLoopContext): Unit = {
    val stackTrace = ex.getStackTrace
    if (!stackTrace.isEmpty) {
      val augmented = stackTrace(stackTrace.length - 1).getClassName.indexOf('@') != -1
      if (!augmented) {
        val prefix = stackTrace.filter(ste => !runLoopFilter.exists(ste.getClassName.startsWith(_)))
        val suffix = ctx
          .getStackTraces()
          .flatMap(t => WIOTraceImpl.getOpAndCallSite(t.stackTrace))
          .map {
            case (methodSite, callSite) =>
              val op = NameTransformer.decode(methodSite.getMethodName)

              new StackTraceElement(
                op + " @ " + callSite.getClassName,
                callSite.getMethodName,
                callSite.getFileName,
                callSite.getLineNumber
              )
          }
          .toArray
        ex.setStackTrace(suffix ++ prefix)
      }
    }
  }  
}