package wio.random

import wio._

final class RandomImpl extends Random {
  private lazy val _rnd = new java.util.Random()
  
  def nextBytes(length: Int): TIO[Array[Byte]] = TIO {
    val arr = new Array[Byte](length)
    _rnd.nextBytes(arr)
    arr
  }

  def nextInt(max: Int): TIO[Int] = TIO {
    _rnd.nextInt(max)
  }

  def nextLong(max: Long): TIO[Long] = TIO {
    val rnd = _rnd.nextLong()
    val rndPct = rnd.toDouble / Long.MaxValue
    (rnd * rndPct).toLong
  }
}