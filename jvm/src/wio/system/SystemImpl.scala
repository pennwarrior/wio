package wio.system

import wio._

class SystemImpl extends System {

  lazy val _console = new ConsoleImpl
  def console: PIO[Console] = PIO.pure(_console)
  def env(name: String): TIO[Option[String]] = TIO(Option(java.lang.System.getenv(name)))
  def property(name: String): TIO[Option[String]] = TIO(Option(java.lang.System.getProperty(name)))
}


final class ConsoleImpl extends Console {
  def errorln(msg: String): PIO[Unit] = PIO(scala.Console.err)
  def println(msg: String): PIO[Unit] = PIO(scala.Console.println(msg))
  def readln: PIO[String] = PIO(scala.Console.in.readLine())

}