#!/bin/sh

sudo apt-get update
sudo apt-get upgrade
sudo apt-get install -y \
	git \
	curl \

echo "### Everything below has been modified by the Vagrant provisioning script ###" >> /home/vagrant/.bashrc

sudo su - vagrant -s /bin/bash -c "curl -Lo cs https://git.io/coursier-cli-linux && chmod +x cs && ./cs setup --yes --jvm 11 --apps scala,scalac,scalafmt,ammonite,cs; rm ./cs"

sudo curl -L https://github.com/lihaoyi/mill/releases/download/0.9.6/0.9.6 > /usr/local/bin/mill && sudo chmod +x /usr/local/bin/mill

sed -i '/^### Everything below has been modified by the Vagrant provisioning script ###/a\
cd /vagrant' /home/vagrant/.bashrc 

if [ -f /vagrant/local/.bashrc ]; then
	cat /vagrant/local/.bashrc >> /home/vagrant/.bashrc
fi

echo ================================================
echo Installing of Vagrant box complete.
echo 
echo Execute "vagrant ssh" to connect to the box.
echo ================================================
