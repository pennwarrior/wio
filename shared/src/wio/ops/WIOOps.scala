package wio.ops

import cats.effect._
import cats.effect.unsafe.IORuntime
import cats.syntax.either._
import cats.{Functor, Monad}
import shapeless.ops.hlist
import shapeless.{::, HList, HNil}
import wio.tracing.WIOTracing
import wio.{TIO, UIO, WIO}

import scala.annotation.nowarn
import scala.annotation.unchecked.uncheckedVariance
import scala.concurrent.Future
import scala.concurrent.duration.FiniteDuration
import scala.reflect.runtime.universe._

import WIO._

/** Extension methods for [[WIO]] */
object WIOOps extends WIOOps

/** Extension methods for [[WIO]] */
trait WIOOps {

  @scala.annotation.nowarn()
  implicit def ensureDeps[W0 <: HList, W <: HList, E, A](
      wio: WIO[W, E, A]
  )(implicit ev1: hlist.Align[W, W0]) = wio.ensure[W0]

  implicit class MonadOps[F[_], W <: HList, +E, +A](wio: WIO[W, E, F[A]])(implicit F : Monad[F]) {
    def subflatMap[B](f: A => F[B]): WIO[W, E, F[B]] =
      wio.map(a => F.flatMap(a)(f))
  }

  implicit class FunctorOps[F[_], W <: HList, +E, +A](wio: WIO[W, E, F[A]])(implicit F : Functor[F]) {
    def submap[B](f: A => B): WIO[W, E, F[B]] =
      wio.map(a => F.map(a)(f))
  }

  /** Functions for when an [[WIO]] has no functional dependencies and the error channel is `Throwable` (i.e.: a [[TIO]]).
    *
    * In this scenario, the cats `IO` execution functions are exposed
    */
  implicit class NoDepsOps[E, A](wio: WIO[HNil, E, A]) {

    /** Lift the [[WIO]] into an [cats.effect.IO]].
      *
      * @see [[WIO#toEitherIO]] when handling non-`Throwable`s.
      */
    def toIO: IO[A] = {
      toEitherIO(wio).flatMap { ea =>
        IO.fromEither {
          ea.leftMap(toThrowable)
        }
      }
    }

    final def unsafeRunSync()(implicit ev: IORuntime): A =
      wio.toIO.unsafeRunSync()

    final def unsafeRunTimed(limit: FiniteDuration)(implicit ev: IORuntime): Option[A] =
      wio.toIO.unsafeRunTimed(limit)

    final def unsafeToFuture()(implicit ev: IORuntime): Future[A] =
      wio.toIO.unsafeToFuture()

    final def timeoutTo[E2 >: E, A2 >: A](duration: FiniteDuration, fallback: TIO[A2]): TIO[A2] =
      for {
        pll <- PullContext(WIOTracing.uncached())
        fib <- PushContext(fromIO(wio.toIO.timeoutTo(duration, fallback.toIO)), pll)
      } yield fib

    final def timeout[A2 >: A](duration: FiniteDuration): TIO[A2] =
      for {
        pll <- PullContext(WIOTracing.uncached())
        fib <- PushContext(fromIO(wio.toIO.timeout(duration)), pll)
      } yield fib

    final def delayBy[A2 >: A](duration: FiniteDuration): TIO[A2] =
      for {
        pll <- PullContext(WIOTracing.uncached())
        fib <- PushContext(fromIO(wio.toIO.delayBy(duration)), pll)
      } yield fib

    final def start: TIO[FiberIO[A @uncheckedVariance]] =
      for {
        pll <- PullContext(WIOTracing.uncached())
        fib <- PushContext(fromIO(wio.toIO.start), pll)
      } yield fib

    final def bracket[E2 >: E, B](
        use: A => UIO[E2, B]
    )(release: A => UIO[E2, Unit]): TIO[B] =
      for {
        pll <- PullContext(WIOTracing.uncached())
        fib <- PushContext(
                 fromIO(
                   wio.toIO.bracket(a => use(a).toIO)(a => release(a).toIO)
                 ),
                 pll
               )
      } yield fib

    def guarantee[E2 >: E, A2 >: A](
        finalizer: UIO[E2, Unit]
    ): TIO[A2] =
      for {
        pll <- PullContext(WIOTracing.uncached())
        fib <- PushContext(fromIO(wio.toIO.guarantee(finalizer.toIO)), pll)
      } yield fib
  }
}

/** Shapeless type class for allowing an instance of a functional dependency to be "injected" into the effect.
  *
  * This is used in the [[WIO#injectSome]] and [[WIO#injectOne]] functions.
  */
trait InjectSome[W <: HList, L <: HList] {
  type Out <: HList
}

object InjectSome {
  def apply[W <: HList, L <: HList](implicit inject: InjectSome[W, L]): Aux[W, L, inject.Out] = inject

  type Aux[W <: HList, L <: HList, Out0] = InjectSome[W, L] { type Out = Out0 }

  @nowarn("msg=parameter value ra in method hlistInjectSomeNil is never used")
  implicit def hlistInjectSomeNil[L <: HList](implicit ra: hlist.RemoveAll[L, HNil]): Aux[L, HNil, L] =
    new InjectSome[L, HNil] {
      type Out = L
    }

  @nowarn("msg=parameter value ra in method hlistInjectSome is never used")
  implicit def hlistInjectSome[W <: HList, E, Rem <: HList, SLT <: HList](implicit
      ra: hlist.RemoveAll.Aux[W, E :: SLT, (E :: SLT, Rem)]
  ): Aux[W, E :: SLT, Rem] =
    new InjectSome[W, E :: SLT] {
      type Out = Rem
    }
}

/** A Shapeless type class for validating that every `HList` entry also contains a Scala `TypeTag`.
  *
  * This is used in the construction of a series of `Require` instances when calling [[WIO.requireMany]].
  */
trait TaggedHList[L <: HList] {
  type Out <: HList

  def buildRequire: WIO[Out, Nothing, Out]
}

object TaggedHList {
  type Aux[L <: HList, Out0 <: HList] = TaggedHList[L] { type Out = Out0 }

  implicit def singleTagged[H <: AnyRef](implicit tag: TypeTag[H]) =
    new TaggedHList[H :: HNil] {
      type Out = H :: HNil

      def buildRequire: WIO[H :: HNil, Nothing, H :: HNil] = WIO.require[H].map(_ :: HNil)
    }

  implicit def hlistTagged[H <: AnyRef : TypeTag, T <: AnyRef : TypeTag, U <: HList, Out0 <: HList, Out1 <: HList](
      implicit
      ct: TaggedHList.Aux[T :: U, Out0],
      u: MergeDeps.Aux[H :: HNil, Out0, Out1]
  ) =
    new TaggedHList[H :: T :: U] {
      type Out = Out1

      def buildRequire: WIO[Out1, Nothing, Out1] =
        for {
          h  <- WIO.require[H]
          tu <- ct.buildRequire
        } yield u(h :: HNil, tu)
    }
}

/** A Shapeless type class for validating that every injected `HList` entry also contains a Scala `TypeTag`.
  *
  * This is used in the construction of a series of `Inject` instances when calling [[WIO.inject]] or [[WIO.injectSome]].
  */
trait TaggedInjectHList[L <: HList] {
  val types: List[String]
}

object TaggedInjectHList {
  private[wio] def stringifyType(t : Type) = (t.typeSymbol.fullName :: " " :: t.typeArgs.map(_.typeSymbol.fullName)).mkString(" ")
  
  implicit def singleTagged[H <: AnyRef](implicit tag: TypeTag[H]) =
    new TaggedInjectHList[H :: HNil] {
      val types = List(stringifyType(tag.tpe.dealias))
    }

  implicit def hlistTagged[H <: AnyRef, T <: AnyRef : TypeTag, U <: HList, Out0 <: HList](implicit
    tag: TypeTag[H],  
    ct: TaggedInjectHList[T :: U]
  ) =
    new TaggedInjectHList[H :: T :: U] {
      val types = scala.collection.immutable.::(stringifyType(tag.tpe.dealias), ct.types)
    }
}
