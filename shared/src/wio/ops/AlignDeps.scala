package wio.ops

import shapeless.HList

/** Type class supporting the alignment of two WIO dependency types, taken from Shapeless's `Align` typeclass.
  * 
  * @note This must be used instead of the `Align` typeclass due to implicit resolution issues on edge cases
  * when both WIO instances have the same dependencies.  Shapeless has implicits readily available
  * for when the other WIO instance has known dependencies.  However, if the second instance has unknown dependencies
  * (i.e.: an abstract type, `W`, only known to be an `HList`), Shapeless has no implicitly available `Union` instance.
  * There is no way for WIO to fill that gap without conflicting with Shapeless on the former case.
  */
trait AlignDeps[L <: HList, M <: HList] {
  def apply(l: L): M
}

object AlignDeps extends AlignDepsInstances0

sealed trait AlignDepsInstances0 extends AlignDepsInstances1 {
  implicit def alignSame[L <: HList]: AlignDeps[L, L] = 
    new AlignDeps[L, L] {
      def apply(l: L): L = l
    }
}

sealed trait AlignDepsInstances1 {
  implicit def hlistAlign[L <: HList, M <: HList](implicit ev: shapeless.ops.hlist.Align[L, M]) = 
    new AlignDeps[L, M] {
      def apply(l: L): M = ev(l)
    }
}