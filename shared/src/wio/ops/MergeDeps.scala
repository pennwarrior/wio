package wio.ops

import shapeless.{HList, HNil}
import shapeless.ops.hlist.Union

/** Type class supporting the union of two WIO dependency types, taken from Shapeless's `Union` typeclass.
  * 
  * @note This must be used instead of the `Union` typeclass due to implicit resolution issues on edge cases
  * when one WIO instance has no depedencies (i.e.: is `HNil`).  Shapeless has implicits readily available
  * for when the other WIO instance has known dependencies.  However, if the second instance has unknown dependencies
  * (i.e.: an abstract type, `W`, only known to be an `HList`), Shapeless has no implicitly available `Union` instance.
  * There is no way for WIO to fill that gap without conflicting with Shapeless on the former case.
  */
trait MergeDeps[L <: HList, M <: HList] {
  type Out <: HList

  def apply(l: L, m: M): Out
}

object MergeDeps extends MergeDepsInstances0 {
  type Aux[L <: HList, M <: HList, Out1 <: HList] = MergeDeps[L, M] { type Out = Out1 }
}

sealed private[wio] trait MergeDepsInstances0 extends MergeDepsInstances1 {
  implicit def hlistNil[M <: HList]: MergeDeps.Aux[HNil, HNil, HNil] =
    new MergeDeps[HNil, HNil] {
      type Out = HNil
      def apply(l: HNil, m: HNil): Out = HNil
    }  
}

sealed private[wio] trait MergeDepsInstances1 extends MergeDepsInstances2 {
  implicit def hlistLeftNil[M <: HList]: MergeDeps.Aux[M, HNil, M] =
    new MergeDeps[M, HNil] {
      type Out = M
      def apply(l: M, m: HNil): Out = l
    }  

  implicit def hlistRightNil[M <: HList]: MergeDeps.Aux[HNil, M, M] =
    new MergeDeps[HNil, M] {
      type Out = M
      def apply(l: HNil, m: M): Out = m
    }  
}

sealed private[wio] trait MergeDepsInstances2 {
  implicit def hlistMergeDeps[L <: HList, M <: HList, U <: HList](implicit u: Union.Aux[L, M, U]): MergeDeps.Aux[L, M, U] =
    new MergeDeps[L, M] {
      type Out = U
      def apply(l: L, m: M): Out = u.apply(l, m)
    }
}