package wio

import cats.arrow._
import cats.effect._
import cats.{Applicative, Eval, NonEmptyParallel}
import shapeless.{::, HList, HNil}
import wio.instances.WIOInstances
import wio.internals.{RunLoop, RunLoopContext}
import wio.ops._
import wio.tracing.{IOEvent, WIOTrace, WIOTracing}

import scala.concurrent.Future
import scala.concurrent.duration.FiniteDuration
import scala.reflect.runtime.universe._
import scala.util.{Failure, Success, Try}

import ops.{InjectSome, TaggedHList, TaggedInjectHList}

/** Wrapper around [[https://typelevel.org/cats-effect/api/2.x/cats/effect/IO.html cats.effect.IO]] to track the error channel and dependencies separately */
sealed abstract class WIO[W <: HList, +E, +A] {
  import WIO._

  /**
    * @see [[https://typelevel.org/cats-effect/api/2.x/cats/effect/IO.html#as[B](newValue:=>B):cats.effect.IO[B] cats.effect.IO.as]]
    */
  def as[B](newValue: => B): WIO[W, E, B] = __map(_ => newValue)

  /** Transforms the value inside the `WIO` effect to a new value.
    *
    * @see [[https://typelevel.org/cats-effect/api/2.x/cats/effect/IO.html#map[B](f:A=%3EB):cats.effect.IO[B] cats.effect.IO.map]]
    */
  final def map[B](f: A => B): WIO[W, E, B] = __map(f)

  private[wio] def __map[B](f: A => B, cachedTrace: Boolean = true): WIO[W, E, B] =
    Map(this, f, if (cachedTrace) WIOTracing.cached(f.getClass) else WIOTracing.uncached())

  private[wio] def __mapUntraced[B](f: A => B): WIO[W, E, B] =
    Map(this, f, null)

  /**
    * @see [[https://typelevel.org/cats-effect/api/2.x/cats/effect/IO.html#flatMap[B](f:A=%3Ecats.effect.IO[B]):cats.effect.IO[B] cats.effect.IO.flatMap]]
    */
  final def flatMap[L <: HList, H >: E, B](f: A => WIO[L, H, B])(implicit u: MergeDeps[W, L]): WIO[u.Out, H, B] =
    __flatMap(f)

  private def __flatMap[L <: HList, H >: E, B](f: A => WIO[L, H, B], cachedTrace: Boolean = true)(implicit
      u: MergeDeps[W, L]
  ): WIO[u.Out, H, B] =
    FlatMapD(this, f, if (cachedTrace) WIOTracing.cached(f.getClass) else WIOTracing.uncached())

  /** Executes a flatMap using the given function, __keeping__ the same dependecy requirement, `W` */
  final def flatMapW[H >: E, B](f: A => WIO[W, H, B]): WIO[W, H, B] =
    FlatMap(this, f, WIOTracing.cached(f.getClass))

  /** Transforms the error into a new error */
  final def mapError[H](f: E => H): WIO[W, H, A] = MapError(this, f, WIOTracing.cached(f.getClass))

  /** Monadic bind on IO's error channel, ensuring sequential execution.  This is the error channel's correlary
    * to the value channel's `flatMap` operation.
    *
    * @see [[https://typelevel.org/cats-effect/api/2.x/cats/effect/IO.html#flatMap[B](f:A=%3Ecats.effect.IO[B]):cats.effect.IO[B] cats.effect.IO.flatMap]]
    *
    */
  final def flatMapError[L <: HList, H, B >: A](f: E => WIO[L, H, B])(implicit u: MergeDeps[W, L]): WIO[u.Out, H, B] =
    FlatMapErrorD(this, f, WIOTracing.cached(f.getClass))

  /** Executes a flatMapError using the given function, __keeping__ the same dependency requirement, `W` */
  final def flatMapErrorK[H, B >: A](f: E => WIO[W, H, B]): WIO[W, H, B] =
    FlatMapError(this, f, WIOTracing.cached(f.getClass))

  /** Cohere the error and value channels of the effect, returning the new value */
  final def fold[B](ef: E => B, af: A => B): WIO[W, Nothing, B] =
    Fold(this, ef, af, WIOTracing.cached(ef.getClass), WIOTracing.cached(af.getClass))

  /** Effectfully cohere the error and value channels of the effect, returning the new value, __keeping__ the smae dependency requirement, `W`. */
  final def foldWith[H, B] = new FoldWithPartial[H, B](this)

  private[wio] class FoldWithPartial[H, B](io: WIO[W, E, A]) {
    def apply[EE <: H, EA <: H, BE <: B, BA <: B](
        ef: E => WIO[W, EE, BE],
        af: A => WIO[W, EA, BA]
    ): WIO[W, H, B] = FoldWith(io, ef, af, WIOTracing.cached(ef.getClass), WIOTracing.cached(af.getClass))
  }

  /**
    * @see [[https://typelevel.org/cats-effect/api/2.x/cats/effect/IO.html#handleErrorWith[AA%3E:A](f:Throwable=%3Ecats.effect.IO[AA]):cats.effect.IO[AA] cats.effect.IO.handleErrorWith]]
    */
  final def handleError[B >: A](f: E => B): WIO[W, Nothing, B] =
    flatMapError(e => WIO.pure(f(e)))

  /** Alias for [[#flatMapError]]
    *
    * @see [[https://typelevel.org/cats-effect/api/2.x/cats/effect/IO.html#handleErrorWith[AA%3E:A](f:Throwable=%3Ecats.effect.IO[AA]):cats.effect.IO[AA] cats.effect.IO.handleErrorWith]]
    */
  final def handleErrorWith[L <: HList, H, AA >: A](
      f: E => WIO[L, H, AA]
  )(implicit u: MergeDeps[W, L]): WIO[u.Out, H, AA] =
    flatMapError(f)

  private[wio] final def handleErrorWithS[H, AA >: A](f: E => WIO[W, H, AA]): WIO[W, H, AA] =
    flatMapErrorK(f)

  /**
    * @see [[https://typelevel.org/cats-effect/api/2.x/cats/effect/IO.html#uncancelable:cats.effect.IO[A] cats.effect.IO.uncancelable]]
    */
  final def uncancelable[A2 >: A](implicit ev: E <:< Throwable): WIO[HNil, Throwable, A2] =
    fromIO(toEitherIO(this).uncancelable).__flatMap(
      {
        case Left(value)  => WIO.raiseError(ev(value))
        case Right(value) => WIO.pure(value)
      },
      false
    )

  /**
    * @see [[https://typelevel.org/cats-effect/api/2.x/cats/effect/IO.html#option:cats.effect.IO[Option[A]] cats.effect.IO.option]]
    */
  def option: WIO[W, E, Option[A]] = __map(Some(_), false)

  /**
    * @see [[https://typelevel.org/cats-effect/api/2.x/cats/effect/IO.html#parProduct[B](another:cats.effect.IO[B])(implicitp:cats.NonEmptyParallel[cats.effect.IO]):cats.effect.IO[(A,B)] cats.effect.IO.parProduct]]
    */
  def parProduct[E2 >: E, B](another: WIO[W, E2, B])(implicit
      p: NonEmptyParallel[({ type XIO[X] = WIO[W, E2, X] })#XIO]
  ): WIO[W, E2, (A, B)] = p.sequential(p.apply.product(p.parallel(this), p.parallel(another)))

  /** Alias for [[#fold]]
    *
    * @see [[https://typelevel.org/cats-effect/api/2.x/cats/effect/IO.html#redeem[B](recover:Throwable=%3EB,map:A=%3EB):cats.effect.IO[B] cats.effect.IO.redeem]]
    */
  def redeem[B](recover: E => B, map: A => B): WIO[W, Nothing, B] = fold(recover, map)

  /** Alias for [[#foldWith]]
    *
    * @see [https://typelevel.org/cats-effect/api/2.x/cats/effect/IO.html#redeemWith[B](recover:Throwable=%3Ecats.effect.IO[B],bind:A=%3Ecats.effect.IO[B]):cats.effect.IO[B] cats.effect.IO.redeemWith]]
    */
  def redeemWith[H >: E, B](recover: H => WIO[W, H, B], bind: A => WIO[W, H, B]): WIO[W, H, B] =
    foldWith(recover, bind)

  /** Satisfy one of the required dependencies by injecting an instance of the dependency into the effect.
    *
    * @note The dependency must be a type present in this instance of `WIO`'s dependency type parameter, `W`
    *
    * @param di The instance of a dependency to inject
    */
  def injectOne[L](di: L)(implicit r: InjectSome[W, L :: HNil], tags: TaggedInjectHList[L :: HNil]): WIO[r.Out, E, A] =
    injectSome(di :: HNil)

  /** Satisfy some of the required dependencies by injecting an instance of the dependencies into the effect.
    *
    * @note The dependencies must be types present in this instance of `WIO`'s dependency type parameter, `W`
    *
    * @param di The instances of dependencies to inject within a Shapeless `HList`
    */
  def injectSome[L <: HList](di: L)(implicit r: InjectSome[W, L], tags: TaggedInjectHList[L]): WIO[r.Out, E, A] =
    InjectPartial(this, di, tags)

  /** Satisfy all of the required dependencies by injecting them into the effect.  This sinks the dependency
    * type parameter to `HNil`.
    *
    * @param di The instances of dependencies to inject within a Shapeless `HList`
    */
  def inject[L <: HList](di: L)(implicit tags: TaggedInjectHList[W], align: AlignDeps[W, L]): UIO[E, A] =
    Inject(this, di, tags, align)

  @scala.annotation.nowarn()
  def ensure[W0 <: HList](implicit ev1: AlignDeps[W, W0]): WIO[W0, E, A] =
    Ensure[W0, W, E, A](this)

  override def toString: String =
    this match {
      case error: Error[E] => s"WIO(Error: $error)"
      case a: Pure[A]      => s"WIO($a)"
      case _               => "WIO$" + System.identityHashCode(this)
    }

  /** Returns a `Unit` without performing any other operations */
  def void: WIO[W, E, Unit] = __map(_ => (), false)

  def widen[E0 >: E, A0 >: A]: WIO[W, E0, A0] = this.asInstanceOf[WIO[W, E0, A0]]

  def widenW[W0 <: HList](implicit ev: W =:= HNil): WIO[W0, E, A] = this.asInstanceOf[WIO[W0, E, A]]

  def widenAll[W0 <: HList, E0 >: E, A0 >: A](implicit ev: W =:= HNil): WIO[W0, E0, A0] =
    this.asInstanceOf[WIO[W0, E0, A0]]

  def swap: WIO[W, A, E] =
    foldWith(
      e => WIO.pure(e),
      v => WIO.raiseError(v)
    )

  /**
    * @see [[https://typelevel.org/cats-effect/api/2.x/cats/effect/IO.html#*%3E[B](another:cats.effect.IO[B]):cats.effect.IO[B] cats.effect.IO.*>]]
    */
  def *>[L <: HList, H >: E, B](another: WIO[L, H, B])(implicit u: MergeDeps[W, L]): WIO[u.Out, H, B] =
    __flatMap(_ => another, false)

  /**
    * @see [[https://typelevel.org/cats-effect/api/2.x/cats/effect/IO.html#%3C*[B](another:cats.effect.IO[B]):cats.effect.IO[A] cats.effect.IO.<*]]
    */
  def <*[L <: HList, H >: E, B](another: WIO[L, H, B])(implicit u: MergeDeps[W, L]): WIO[u.Out, H, A] =
    __flatMap(another.as(_), false)

  /**
    * @see [[https://typelevel.org/cats-effect/api/2.x/cats/effect/IO.html#&%3E[B](another:cats.effect.IO[B])(implicitp:cats.NonEmptyParallel[cats.effect.IO]):cats.effect.IO[B] cats.effect.IO.&>]]
    */
  def &>[H >: E, B](another: WIO[W, H, B])(implicit
      p: NonEmptyParallel[({ type XIO[X] = WIO[W, H, X] })#XIO]
  ): WIO[W, H, B] = p.parProductR(this)(another)

  /**
    * @see [[https://typelevel.org/cats-effect/api/2.x/cats/effect/IO.html#%3C&[B](another:cats.effect.IO[B])(implicitp:cats.NonEmptyParallel[cats.effect.IO]):cats.effect.IO[A] cats.effect.IO.<&]]
    */
  def <&[H >: E, B](another: WIO[W, H, B])(implicit
      p: NonEmptyParallel[({ type XIO[X] = WIO[W, H, X] })#XIO]
  ): WIO[W, H, A] = p.parProductL(this)(another)
}

object WIO extends WIOInstances {

  // Disable built-in Cats Effect tracing since WIO is handling it
  System.setProperty("cats.effect.stackTracingMode", "off")

  /** Delays execution of `a` and returns into an [[WIO]] */
  def apply[A](a: => A): TIO[A] = Delay(() => a)

  /** Lifts the pure value, `a`, into an [[WIO]] */
  def pure[A](a: A): UIO[Nothing, A] = Pure(a)

  def defer[W <: HList, E, A](a: => WIO[W, E, A]) = Suspend(() => a)

  /** Returns an `WIO` with the error channel set to `e`. */
  def raiseError[E](e: E): UIO[E, Nothing] = WIO.traceEffect(Error(e))

  /** Lifts an `cats.effect.IO` into an [[WIO]] */
  def fromIO[A](ceio: cats.effect.IO[A]): TIO[A] = WIO.traceEffect(CEIO(ceio))

  /** Returns a pure `WIO[HNil, Nothing, Unit]` */
  val unit = pure(())

  /** Lifts an `Option[A]` set to `None` into a [[WIO]] */
  def none[A] = pure(None: Option[A])

  /** Return an instance of type `W` to be used in a future operation and mark this type as a dependency of the effect. */
  def require[W <: AnyRef](implicit ev: TypeTag[W]): WIO[W :: HNil, Nothing, W] = Require(ev)

  /** Return an instance of type `W` to be used in a future operation and mark this type as a dependency of the effect. */
  def requireMany[W <: HList](implicit ev: TaggedHList[W]) = ev.buildRequire

  /**
    * @see [[https://typelevel.org/cats-effect/api/2.x/cats/effect/IO$.html#eval[A](fa:cats.Eval[A]):cats.effect.IO[A] cats.effect.IO$.eval]]
    */
  def eval[A](fa: Eval[A]): TIO[A] = fromIO(IO.eval(fa))

  def fromFuture[A](fut: Future[A]): TIO[A] =
    fromIO(IO.fromFuture(IO(fut)))

  /** Lifts an `Either` into an [[WIO]] */
  def fromEither[E, A](e: Either[E, A]): WIO[HNil, E, A] =
    e match {
      case Right(value) => pure(value)
      case Left(value)  => raiseError(value)
    }

  /** Lifts an `Option[A]` into an `WIO[W, E, A]`
    *
    * If `None`, lifts the value of the given error in `orElse`
    */
  def fromOption[E, A](option: Option[A])(orElse: => E): UIO[E, A] =
    option match {
      case Some(value) => pure(value)
      case None        => raiseError(orElse)
    }

  /** Lifts an `Try[A]` into a `TIO[A]` */
  def fromTry[A](t: Try[A]): TIO[A] =
    t match {
      case Success(a)   => pure(a)
      case Failure(err) => raiseError(err)
    }

  /**
    * @see [[https://typelevel.org/cats-effect/api/2.x/cats/effect/IO$.html#never:cats.effect.IO[Nothing]]]
    */
  val never: TIO[Nothing] = fromIO(IO.never)

  /** Asynchronously sleeps for the given duration */
  def sleep(duration: FiniteDuration)(implicit ev: Temporal[TIO]): TIO[Unit] = ev.sleep(duration)

  /**
    * @see [[https://typelevel.org/cats-effect/api/2.x/cats/effect/IO$.html#trace:cats.effect.IO[cats.effect.tracing.IOTrace] cats.effect.IO$.trace]]
    */
  def trace: TIO[WIOTrace] = DumpTrace(WIOTracing.buildStackTrace)

  /** Executes the `ifCond` effect if the given condition is true.  Otherwise executes the `elseCond` condition.
    *
    * This is the type safe way of doing `if (cond) ifCond else elseCond`
    * 
    * @example `WIO.ifA(true).thenA(WIO.unit).elseA(WIO.raiseError(err))`
    */
  def ifA(cond: Boolean) = new PartialIfCondition(cond)

  final class PartialIfCondition private[wio] (cond: Boolean) {
    def thenA[W <: HList, E, A](ifCond: => WIO[W, E, A]) = new PartialIfThenCondition[W, E, A](cond, ifCond)
  }

  final class PartialIfThenCondition[W1 <: HList, E1, A1] private[wio] (cond: Boolean, ifCond: => WIO[W1, E1, A1]) {
    def elseA[W2 <: HList, E3 >: E1, A3 >: A1, E2 <: E3, A2 <: A3](elseCond: => WIO[W2, E2, A2])
      (implicit dep1: MergeDeps[W1, W2]) =  
          (if (cond)
            ifCond
          else
            elseCond).asInstanceOf[WIO[dep1.Out, E3, A3]]
  }

  /** Returns the action if the condition is true */
  def whenA[W <: HList, E](cond: Boolean)(action: => WIO[W, E, Unit]): WIO[W, E, Unit]   =
    Applicative[({ type XIO[X] = WIO[W, E, X] })#XIO].whenA(cond)(action)

  /** Returns the action if the condition is false */
  def unlessA[W <: HList, E](cond: Boolean)(action: => WIO[W, E, Unit]): WIO[W, E, Unit] =
    Applicative[({ type XIO[X] = WIO[W, E, X] })#XIO].unlessA(cond)(action)

  /** Returns the error if the condition is true, otherwise returns unit */
  def raiseWhen[E](cond: Boolean)(error: => E): UIO[E, Unit]                             =
    whenA(cond)(raiseError(error))

  /** Returns the error if the condition is false, otherwise returns unit */
  def raiseUnless[E](cond: Boolean)(error: => E): UIO[E, Unit] =
    unlessA(cond)(raiseError(error))

  /** [[https://typelevel.org/cats/datatypes/functionk.html cats.arrow.FunctionK]] to convert from an `IO` into a [[TIO]]
    *
    * @see [[SIOOps#ThrowableOps#fromIO]]
    */
  val FromIO = new FunctionK[IO, TIO] {
    def apply[A](fa: IO[A]): TIO[A] = fromIO(fa)
  }

  /** [[https://typelevel.org/cats/datatypes/functionk.html cats.arrow.FunctionK]] to convert from a [[TIO]] into an `IO`
    *
    * @see [[SIOOps#ThrowableOps#toIO]]
    */
  val ToIO = new FunctionK[TIO, IO] {
    def apply[A](fa: TIO[A]): IO[A] = fa.toIO
  }

  implicit class hNilAsAny[W <: HList, +E, +A](wio: WIO[HNil, E, A]) extends WIO[W, E, A] {
    implicit val kdep = wio.asInstanceOf[WIO[W, E, A]]
  }

  private[wio] final case class Pure[+A](a: A) extends WIO[HNil, Nothing, A]

  private[wio] final case class Delay[+A](a: () => A) extends WIO[HNil, Throwable, A]

  private[wio] final case class Suspend[W <: HList, +E, +A](io: () => WIO[W, E, A]) extends WIO[W, E, A]

  private[wio] final case class Require[W <: AnyRef](s: TypeTag[W]) extends WIO[W :: HNil, Nothing, W]

  private[wio] final case class Error[+E](error: E) extends WIO[HNil, E, Nothing]

  private[wio] case class DumpTrace(trace: AnyRef) extends WIO[HNil, Nothing, WIOTrace]

  private[wio] case class PushContext[W <: HList, +E, +A](io: WIO[W, E, A], ctx: RunLoopContext) extends WIO[W, E, A]

  private[wio] case class PullContext(trace: IOEvent) extends WIO[HNil, Nothing, RunLoopContext]

  private[wio] final case class CEIO[+A](cio: IO[A]) extends WIO[HNil, Throwable, A]

  private[wio] final case class Ensure[W0 <: HList, W <: HList, +E, +A](wio: WIO[W, E, A]) extends WIO[W0, E, A]

  private[wio] final case class Trace[W <: HList, +E, +A](wio: WIO[W, E, A], trace: IOEvent) extends WIO[W, E, A]

  private[wio] final case class Map[W <: HList, +E, A, +B](io: WIO[W, E, A], f: A => B, trace: AnyRef)
      extends WIO[W, E, B]
      with (A => WIO[W, E, B]) {
    def apply(a: A): WIO[W, E, B] = Pure(f(a))
  }

  private[wio] final case class MapError[W <: HList, E, +H, +A](io: WIO[W, E, A], f: E => H, trace: AnyRef)
      extends WIO[W, H, A]
      with (E => WIO[W, H, A]) {
    def apply(e: E): WIO[W, H, A] = Error(f(e))
  }

  private[wio] final case class FlatMap[W <: HList, E, +H >: E, A, +B](
      io: WIO[W, E, A],
      f: A => WIO[W, H, B],
      trace: AnyRef
  ) extends WIO[W, H, B]

  private[wio] final case class FlatMapD[W <: HList, L <: HList, E, +H >: E, A, +B, U <: MergeDeps[W, L]](
      io: WIO[W, E, A],
      f: A => WIO[L, H, B],
      trace: AnyRef
  ) extends WIO[U#Out, H, B]

  private[wio] final case class FlatMapError[W <: HList, E, A, +H, +B >: A](
      io: WIO[W, E, A],
      f: E => WIO[W, H, B],
      trace: AnyRef
  ) extends WIO[W, H, B]

  private[wio] final case class FlatMapErrorD[W <: HList, L <: HList, E, A, +H, +B >: A, U <: MergeDeps[W, L]](
      io: WIO[W, E, A],
      f: E => WIO[L, H, B],
      trace: AnyRef
  ) extends WIO[U#Out, H, B]

  private[wio] final case class Inject[W <: HList, L <: HList, +E, +A](
      io: WIO[W, E, A],
      i: L,
      ev1: TaggedInjectHList[W],
      ev2: AlignDeps[W, L]
  ) extends WIO[HNil, E, A]

  private[wio] final case class InjectPartial[W <: HList, L0 <: HList, L1 <: HList, +E, +A](
      io: WIO[W, E, A],
      i: L0,
      ev: TaggedInjectHList[L0]
  ) extends WIO[L1, E, A]

  private[wio] final case class Fold[W <: HList, E, A, B](
      io: WIO[W, E, A],
      ef: E => B,
      af: A => B,
      etrace: AnyRef,
      atrace: AnyRef
  ) extends WIO[W, Nothing, B]
      with (A => WIO[W, E, B]) {
    def apply(a: A): WIO[W, E, B] = Pure(af(a))
  }

  private[wio] final case class FoldWith[
      W <: HList,
      E,
      A,
      B,
      H,
      +EE <: H,
      +EA <: H,
      +BE <: B,
      +BA <: B
  ](
      io: WIO[W, E, A],
      ef: E => WIO[W, EE, BE],
      af: A => WIO[W, EA, BA],
      etrace: AnyRef,
      atrace: AnyRef
  ) extends WIO[W, H, B]

  /** Lift the [[WIO]] into a [cats.effect.IO]] of an `Either`
    *
    * @see [[wio.ops.WIOOps.ThrowableOps#toIO toIO]] for `Throwable` error channels.
    */
  def toEitherIO[W <: HList, E, A](wio: WIO[W, E, A]): IO[Either[E, A]] =
    RunLoop.run(wio).asInstanceOf[IO[Either[E, A]]]

  private[wio] def traceEffect[W <: HList, E, A](wio: WIO[W, E, A]) = WIO.Trace(wio, WIOTracing.buildStackTrace)

  private[wio] def toThrowable[E](e: E): Throwable = {
    e match {
      case e: Throwable => e
      case e            => new WIOThrowable[E](e)
    }
  }
}
