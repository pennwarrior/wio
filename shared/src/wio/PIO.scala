package wio

object PIO {
  /**
    * @see [[WIO$#apply]]
    */
  def apply[A](a: => A): PIO[A] = pure(() => a).__mapUntraced(_())

  /**
    * @see [[WIO$#pure]]
    */
  def pure[A](a: A): PIO[A] = WIO.pure(a)

  /**
    * @see [[WIO$#unit]]
    */
  val unit = WIO.unit

  /**
    * @see [[WIO$#none]]
    */
  def none[A] = WIO.none[A]

  /**
    * @see [[WIO$#whenA]]
    */
  def whenA[E](cond: Boolean)(action: => PIO[Unit]): PIO[Unit] = WIO.whenA(cond)(action)

  /**
    * @see [[WIO$#unlessA]]
    */
  def unlessA[E](cond: Boolean)(action: => PIO[Unit]): PIO[Unit] = WIO.unlessA(cond)(action)
}