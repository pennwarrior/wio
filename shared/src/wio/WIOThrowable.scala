package wio

private[wio] class WIOThrowable[+E](val orig: E) extends Throwable {

  override def getLocalizedMessage(): String                    = orig.toString
  override def getMessage(): String                             = orig.toString
}
