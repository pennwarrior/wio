package wio.random

import wio._

trait Random {
  def nextBytes(length: Int): TIO[Array[Byte]]
  def nextInt(max: Int): TIO[Int]
  def nextLong(max: Long): TIO[Long]
}

object Random {

  /** Returns the current [[Random]] instance */
  def build: PIO[Random] = PIO(new RandomImpl)
}