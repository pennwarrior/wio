package wio.tracing

sealed trait IOEvent

object IOEvent {
  final case class StackTrace(stackTrace: List[StackTraceElement]) extends IOEvent
}