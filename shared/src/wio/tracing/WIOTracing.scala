package wio.tracing

/*
 * == Adapted from Cats Effect ==
 * This file is nearly identical to `cats.effect.internals.IOTracing` from Cats Effect 2.x
 * 
 * Original copyright listed below
 * ==============================
 * Copyright (c) 2017-2021 The Typelevel Cats-effect Project Developers
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import java.util.concurrent.ConcurrentHashMap

/** Provides access to Cats Effect tracing functions */
object WIOTracing {

  def buildStackTrace = IOEvent.StackTrace(new Throwable().getStackTrace().toList)

  def uncached(): IOEvent = buildStackTrace

  def cached(clazz: Class[_]): IOEvent = buildCachedFrame(clazz)

  private def buildCachedFrame(clazz: Class[_]): IOEvent = {
    val currentFrame = frameCache.get(clazz)
    if (currentFrame eq null) {
      val newFrame = buildStackTrace
      frameCache.put(clazz, newFrame)
      newFrame
    } else {
      currentFrame
    }
  }

  /**
   * Global cache for trace frames. Keys are references to lambda classes.
   * Should converge to the working set of traces very quickly for hot code paths.
   */
  private[this] val frameCache: ConcurrentHashMap[Class[_], IOEvent] = new ConcurrentHashMap()
}