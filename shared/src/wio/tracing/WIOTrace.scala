package wio.tracing

import wio.TIO

trait WIOTrace {
  def printFiberTrace(options: PrintingOptions = PrintingOptions.Default): TIO[Unit]
  def showFiberTrace(options: PrintingOptions = PrintingOptions.Default): String
}