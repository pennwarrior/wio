package wio

import cats._
import cats.effect._
import wio.tracing.WIOTrace

import scala.concurrent.Future
import scala.concurrent.duration.FiniteDuration
import scala.util.Try

object TIO extends TIOInstances {

  /**
    * @see [[WIO$#apply]]
    */
  def apply[A](a: => A): TIO[A] = WIO.apply(a)

  /**
    * @see [[WIO$#defer]]
    */
  def defer[E, A](a: => TIO[A]) = WIO.defer(a)

  /**
    * @see [[WIO$#raiseError]]
    */
  def raiseError(e: Throwable): TIO[Nothing] = WIO.raiseError(e)

  /**
    * @see [[WIO$#fromIO]]
    */
  def fromIO[A](ceio: cats.effect.IO[A]): TIO[A] = WIO.fromIO(ceio)

  /**
    * @see [[WIO$#unit]]
    */
  val unit = WIO.unit

  /**
    * @see [[WIO$#none]]
    */
  def none[A] = WIO.none[A]

  /**
    * @see [[WIO$#eval]]
    */
  def eval[A](fa: Eval[A]): TIO[A] = WIO.eval(fa)

  /**
    * @see [[WIO$#fromFuture]]
    */
  def fromFuture[A](fut: Future[A]): TIO[A] = WIO.fromFuture(fut)

  /** Lifts an `Either` into an [[WIO]] */
  def fromEither[E, A](e: Either[E, A]): UIO[E, A] = WIO.fromEither(e)

  /**
    * @see [[WIO$#fromOption]]
    */
  def fromOption[A](option: Option[A])(orElse: => Throwable): TIO[A] = WIO.fromOption(option)(orElse)

  /**
    * @see [[WIO$#fromTry]]
    */
  def fromTry[A](t: Try[A]): TIO[A] = WIO.fromTry(t)

  /**
    * @see [[WIO$#sleep]]
    */
  def sleep(duration: FiniteDuration)(implicit ev: Temporal[TIO]): TIO[Unit] = WIO.sleep(duration)

  /**
    * @see [[WIO$#trace]]
    */
  val trace: TIO[WIOTrace] = WIO.trace

  /**
    * @see [[WIO$#never]]
    */
  val never: TIO[Nothing] = WIO.never

  /**
    * @see [[WIO$#whenA]]
    */
  def whenA(cond: Boolean)(action: => TIO[Unit]): TIO[Unit] = WIO.whenA(cond)(action)

  /**
    * @see [[WIO$#unlessA]]
    */
  def unlessA(cond: Boolean)(action: => TIO[Unit]): TIO[Unit] = WIO.unlessA(cond)(action)
}

private[wio] trait TIOInstances extends TIOInstances1 {

}

private[wio] trait TIOInstances1 {
}
