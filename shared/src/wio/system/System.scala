package wio.system

import wio._

trait System {
  def console: PIO[Console]
  // java.lang.System.err
  // java.lang.System.getProperties()
  // java.lang.System.getProperty("")
  def property(name: String): TIO[Option[String]]
  def env(name: String): TIO[Option[String]]
  // java.lang.System.getenv()
  // java.lang.System.getenv("")
  // java.lang.System.in
  // java.lang.System.out
}

object System {

  /** Returns the current [[System]] instance */
  def build: PIO[System] = PIO(new SystemImpl)
}

trait Console {
  def println(msg: String): PIO[Unit]
  def errorln(msg: String): PIO[Unit]
  def readln: PIO[String]
}