import shapeless.HNil

/** WIO is an opinionated wrapper around the [[https://typelevel.org/cats-effect/ Cats Effect IO]] library.  This presents
  * an alternative to the "Tagless Final" approach, which prioritizes exposing the least-powerful abstraction around each function call.
  * [[WIO]], in contrast, exposes the full power of the `IO` instance to the developer on every call and lifts some additional common
  * types up to the effect itself: the error type and the dependant types (sometimes called as "readers").
  */
package object wio extends ops.WIOOps {
  
  /** "Throwable IO" with no functional dependencies.  Alias for an [[WIO]] with `Throwable` as the error channel, which is the
    * common type returned by libraries interacting with Cats and Cats Effect.
    */
  type TIO[+A] = WIO[HNil, Throwable, A]

  /** [[WIO]] with no functional dependencies 
    * 
    * _U = Unit as the functional dependency_
    */
  type UIO[+E, +A] = WIO[HNil, E, A]

  /** [[WIO]] with a pure value (i.e.: no functional dependencies and no error channel)
    * 
    */
  type PIO[+A] = WIO[HNil, Nothing, A]
}