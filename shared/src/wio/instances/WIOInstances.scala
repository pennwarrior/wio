package wio.instances

import cats.effect.IO
import cats.effect.kernel._
import cats.syntax.either._
import cats.{~>, ApplicativeError, Monad}
import shapeless.HList
import wio.{PIO, TIO, WIO}

private[instances] object WIOInstances {

  private[wio] def monad[W <: HList, E]: Monad[({ type XIO[X] = WIO[W, E, X] })#XIO] =
    new Monad[({ type XIO[X] = WIO[W, E, X] })#XIO] {
      def pure[A](x: A): WIO[W, E, A] = WIO.pure(x).widen[E, A]

      def flatMap[A, B](fa: WIO[W, E, A])(f: A => WIO[W, E, B]): WIO[W, E, B] = fa.flatMapW(f)

      def tailRecM[A, B](a: A)(f: A => WIO[W, E, Either[A, B]]): WIO[W, E, B] =
        WIO.defer(f(a).flatMapW {
          case Right(value) => WIO.fromEither(value.asRight[E]).widenAll[W, E, B]
          case Left(value)  => tailRecM(value)(f)
        })
    }

  private[wio] val sync: Sync[TIO] = {
    val ioSync = Sync[IO]

    new Sync[TIO] {
      // Members declared in cats.Applicative
      def pure[A](x: A): wio.TIO[A] = PIO.pure(x)

      // Members declared in cats.ApplicativeError
      def handleErrorWith[A](fa: wio.TIO[A])(f: Throwable => wio.TIO[A]): wio.TIO[A] = fa.flatMapError(f)
      def raiseError[A](e: Throwable): wio.TIO[A]                                    = WIO.raiseError(e)

      // Members declared in Clock
      def monotonic = WIO.fromIO(ioSync.monotonic)
      def realTime  = WIO.fromIO(ioSync.realTime)

      // Members declared in cats.FlatMap
      def flatMap[A, B](fa: wio.TIO[A])(f: A => wio.TIO[B]): wio.TIO[B]   = fa.flatMap(f)
      def tailRecM[A, B](a: A)(f: A => wio.TIO[Either[A, B]]): wio.TIO[B] = monad.tailRecM(a)(f)

      // Members declared in MonadCancel
      def canceled: wio.TIO[Unit]                                     = WIO.fromIO(ioSync.canceled)
      def forceR[A, B](fa: wio.TIO[A])(fb: wio.TIO[B]): wio.TIO[B]    = WIO.fromIO(ioSync.forceR(fa.toIO)(fb.toIO))
      def onCancel[A](fa: wio.TIO[A], fin: wio.TIO[Unit]): wio.TIO[A] = WIO.fromIO(ioSync.onCancel(fa.toIO, fin.toIO))
      def rootCancelScope: CancelScope                                = ioSync.rootCancelScope
      def uncancelable[A](body: Poll[wio.TIO] => wio.TIO[A]): wio.TIO[A] = {
        val ioBody = { ioPoll: Poll[IO] => body(io2tioPoll(ioPoll)).toIO }
        WIO.fromIO(ioSync.uncancelable(ioBody))
      }

      // Members declared in Sync
      def suspend[A](hint: Sync.Type)(thunk: => A): wio.TIO[A] = WIO.fromIO(ioSync.suspend(hint)(thunk))
    }
  }

  private[wio] val async: Async[TIO] =
    new Async[TIO] {

      val ioAsync = Async[IO]

      // Members declared in cats.Applicative
      def pure[A](x: A): wio.TIO[A] = WIO.pure(x)

      // Members declared in cats.ApplicativeError
      def handleErrorWith[A](fa: wio.TIO[A])(f: Throwable => wio.TIO[A]): wio.TIO[A] = fa.handleErrorWith(f)
      def raiseError[A](e: Throwable): wio.TIO[A]                                    = WIO.raiseError(e)

      // Members declared in Clock
      def monotonic: wio.TIO[scala.concurrent.duration.FiniteDuration] = WIO.fromIO(ioAsync.monotonic)
      def realTime: wio.TIO[scala.concurrent.duration.FiniteDuration]  = WIO.fromIO(ioAsync.realTime)

      // Members declared in cats.FlatMap
      def flatMap[A, B](fa: wio.TIO[A])(f: A => wio.TIO[B]): wio.TIO[B] = fa.flatMap(f)
      def tailRecM[A, B](a: A)(f: A => wio.TIO[Either[A, B]]): wio.TIO[B] = {
        val fIO = { a: A => f(a).toIO }
        WIO.fromIO(ioAsync.tailRecM(a)(fIO))
      }

      // Members declared in MonadCancel
      def canceled: wio.TIO[Unit]                                     = WIO.fromIO(ioAsync.canceled)
      def forceR[A, B](fa: wio.TIO[A])(fb: wio.TIO[B]): wio.TIO[B]    = WIO.fromIO(ioAsync.forceR(fa.toIO)(fb.toIO))
      def onCancel[A](fa: wio.TIO[A], fin: wio.TIO[Unit]): wio.TIO[A] = WIO.fromIO(ioAsync.onCancel(fa.toIO, fin.toIO))

      def uncancelable[A](body: Poll[wio.TIO] => wio.TIO[A]): wio.TIO[A] = {
        val ioBody = { ioPoll: Poll[IO] => body(io2tioPoll(ioPoll)).toIO }
        WIO.fromIO(ioAsync.uncancelable(ioBody))
      }

      // Members declared in Sync
      def suspend[A](hint: Sync.Type)(thunk: => A): wio.TIO[A] = WIO.fromIO(ioAsync.suspend(hint)(thunk))

      // Members declared in GenConcurrent
      def deferred[A]: wio.TIO[Deferred[wio.TIO, A]] =
        WIO.fromIO(ioAsync.deferred[A].map(_.mapK(WIO.FromIO)))

      def ref[A](a: A): wio.TIO[Ref[wio.TIO, A]] =
        WIO.fromIO(ioAsync.ref(a).map(_.mapK(WIO.FromIO)))

      // Members declared in GenSpawn
      def cede: wio.TIO[Unit] = WIO.fromIO(ioAsync.cede)

      def start[A](fa: wio.TIO[A]): wio.TIO[Fiber[wio.TIO, Throwable, A]] =
        WIO.fromIO(ioAsync.start(fa.toIO).map(io2tioFiber))

      // Members declared in Async
      def cont[K, R](body: Cont[wio.TIO, K, R]): wio.TIO[R] =
        WIO.fromIO(ioAsync.cont(tio2ioCont(body)))

      def evalOn[A](fa: wio.TIO[A], ec: scala.concurrent.ExecutionContext): wio.TIO[A] =
        WIO.fromIO(ioAsync.evalOn(fa.toIO, ec))

      def executionContext: wio.TIO[scala.concurrent.ExecutionContext] = WIO.fromIO(ioAsync.executionContext)

      // Members declared in GenTemporal
      def sleep(time: scala.concurrent.duration.FiniteDuration): wio.TIO[Unit] = WIO.fromIO(ioAsync.sleep(time))
    }

  private def tio2ioCont[K, R](tio: Cont[TIO, K, R]) = {
    new Cont[IO, K, R] {
      def apply[G[_]](implicit G: MonadCancel[G, Throwable]): (Either[Throwable, K] => Unit, G[K], IO ~> G) => G[R] = {
        case (cancelFn, eff, fk) => {
          val fn = tio.apply
          fn(cancelFn, eff, fk.compose(WIO.ToIO))
        }
      }
    }
  }

  private def io2tioFiber[A](io: Fiber[IO, Throwable, A]) = {
    new Fiber[TIO, Throwable, A] {
      def cancel: TIO[Unit] = WIO.fromIO(io.cancel)

      def join: TIO[Outcome[TIO, Throwable, A]] = WIO.fromIO(io.join.map(_.mapK(WIO.FromIO)))
    }
  }

  private def io2tioPoll(io: Poll[IO]) = {
    new Poll[TIO] {
      def apply[A](fa: TIO[A]): TIO[A] = {
        WIO.fromIO(io.apply(fa.toIO))
      }
    }
  }
}

/** Cats type class instances for [[WIO]] */
trait WIOInstances extends SIOLowPriorityInstances0 {
  implicit def catsWioAsync[W <: HList, E](implicit
      ev: E =:= Throwable,
      ev2: W =:= shapeless.HNil
  ): Async[({ type XIO[X] = WIO[W, E, X] })#XIO] =
    WIOInstances.async.asInstanceOf[Async[({ type XIO[X] = WIO[W, E, X] })#XIO]]
}

private[instances] trait SIOLowPriorityInstances0 extends SIOLowPriorityInstances1 {
  implicit def catsWioSync[W <: HList, E](implicit
      ev: E =:= Throwable,
      ev2: W =:= shapeless.HNil
  ): Sync[({ type XIO[X] = WIO[W, E, X] })#XIO] =
    WIOInstances.sync.asInstanceOf[Sync[({ type XIO[X] = WIO[W, E, X] })#XIO]]
}

private[instances] trait SIOLowPriorityInstances1 extends SIOLowPriorityInstances2 {
  import WIOInstances._

  implicit def catsWioApplicativeError[W <: HList, E] =
    new ApplicativeError[({ type XIO[X] = WIO[W, E, X] })#XIO, E] {
      def ap[A, B](ff: WIO[W, E, A => B])(fa: WIO[W, E, A]): WIO[W, E, B] = monad.ap(ff)(fa)
      def pure[A](x: A): WIO[W, E, A]                                     = monad.pure(x)

      def raiseError[A](e: E): WIO[W, E, A] = WIO.raiseError(e).widen[E, A]

      def handleErrorWith[A](fa: WIO[W, E, A])(f: E => WIO[W, E, A]): WIO[W, E, A] = fa.handleErrorWithS(f)

    }
}

private[instances] trait SIOLowPriorityInstances2 {
  implicit def catsWioMonad[W <: HList, E]: Monad[({ type XIO[X] = WIO[W, E, X] })#XIO] = WIOInstances.monad
}
