package wio

import cats._
import cats.effect._
import wio.tracing.WIOTrace

import scala.concurrent.Future
import scala.concurrent.duration.FiniteDuration
import scala.util.Try

object UIO {
  /**
    * @see [[WIO$#pure]]
    */
  def apply[A](a: => A): UIO[Nothing, A] = pure(() => a).__mapUntraced(_())

  /**
    * @see [[WIO$#pure]]
    */
  def pure[A](a: A): UIO[Nothing, A] = WIO.pure(a)

  /**
    * @see [[WIO$#defer]]
    */
  def defer[E, A](a: => UIO[E, A]) = WIO.defer(a)

  /**
    * @see [[WIO$#raiseError]]
    */
  def raiseError[E](e: E): UIO[E, Nothing] = WIO.raiseError(e)

  /**
    * @see [[WIO$#fromIO]]
    */
  def fromIO[A](ceio: cats.effect.IO[A]): TIO[A] = WIO.fromIO(ceio)

  /**
    * @see [[WIO$#unit]]
    */
  val unit = WIO.unit

  /**
    * @see [[WIO$#none]]
    */
  def none[A] = WIO.none[A]

  /**
    * @see [[WIO$#eval]]
    */
  def eval[A](fa: Eval[A]): TIO[A] = WIO.eval(fa)

  /**
    * @see [[WIO$#fromFuture]]
    */
  def fromFuture[A](fut: Future[A]): TIO[A] = WIO.fromFuture(fut)

  /** Lifts an `Either` into an [[WIO]] */
  def fromEither[E, A](e: Either[E, A]): UIO[E, A] = WIO.fromEither(e)

  /**
    * @see [[WIO$#fromOption]]
    */
  def fromOption[E, A](option: Option[A])(orElse: => E): UIO[E, A] = WIO.fromOption(option)(orElse)

  /**
    * @see [[WIO$#fromTry]]
    */
  def fromTry[A](t: Try[A]): TIO[A] = WIO.fromTry(t)

  /**
    * @see [[WIO$#sleep]]
    */
  def sleep(duration: FiniteDuration)(implicit ev: Temporal[TIO]): TIO[Unit] = WIO.sleep(duration)

  /**
    * @see [[WIO$#trace]]
    */
  val trace: TIO[WIOTrace] = WIO.trace

  /**
    * @see [[WIO$#whenA]]
    */
  def whenA[E](cond: Boolean)(action: => UIO[E, Unit]): UIO[E, Unit] = WIO.whenA(cond)(action)

  /**
    * @see [[WIO$#unlessA]]
    */
  def unlessA[E](cond: Boolean)(action: => UIO[E, Unit]): UIO[E, Unit] = WIO.unlessA(cond)(action)

  /**
    * @see [[WIO$#raiseWhen]]
    */
  def raiseWhen[E](cond: Boolean)(error: => E): UIO[E, Unit] = WIO.raiseWhen(cond)(error)

  /**
    * @see [[WIO$#raiseUnless]]
    */
  def raiseUnless[E](cond: Boolean)(error: => E): UIO[E, Unit] = WIO.raiseUnless(cond)(error)

}