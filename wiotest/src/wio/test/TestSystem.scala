package wio.test

import wio.system.System
import wio.system
import wio.PIO
import wio.TIO
import scala.collection.mutable.HashMap
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.Queue

final class TestSystem extends System {

  private val propMap = HashMap.empty[String, String]
  private val envMap = HashMap.empty[String, String]

  def console: PIO[system.Console] = PIO.pure(new TestConsole)

  def setTestProperty(name: String, value: String) = {
    propMap.addOne((name, value))
  }

  def setTestEnv(name: String, value: String) = {
    envMap.addOne((name, value))
  }

  def property(name: String): TIO[Option[String]] = PIO.pure(propMap.get(name))

  def env(name: String): TIO[Option[String]] = PIO.pure(envMap.get(name))
}

final class TestConsole extends system.Console {

  private val _consoleBuff = ListBuffer.empty[String]
  private val _errBuff = ListBuffer.empty[String]
  private val _readQ = Queue.empty[String]

  def consoleOutput = {
    val out = _consoleBuff.toList
    _consoleBuff.clear()
    out
  }

  def errorOutput = {
    val out = _errBuff.toList
    _errBuff.clear()
    out
  }

  def addInput(inputLine : String) = {
    _readQ.enqueue(inputLine)
    ()
  }

  def println(msg: String): PIO[Unit] = PIO(_consoleBuff.append(msg))
  
  def errorln(msg: String): PIO[Unit] = PIO(_errBuff.append(msg))
  
  def readln: PIO[String] = {
    if (_readQ.isEmpty) PIO.pure("")
    else PIO.pure(_readQ.dequeue())
  }
}
