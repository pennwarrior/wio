package wio.test

import wio.random.Random
import wio.{PIO, TIO}

final class TestRandom extends Random {

  private var _bytes: Array[Byte] = Array.empty
  private var _long: Long = 0L
  private var _int: Int = 0

  def setBytes(arr: Array[Byte]) = {
    _bytes = arr
  }

  def setLong(l: Long) = _long = l

  def setInt(i: Int) = _int = i

  def nextBytes(length: Int): TIO[Array[Byte]] = {
    if (_bytes.length < length) TIO.raiseError(new RuntimeException(s"Test array is too small.  Use `setBytes` to make it larger."))
    else PIO.pure(_bytes.take(length))
  }

  def nextInt(max: Int): TIO[Int] = PIO.pure(Math.min(_int, max))
  def nextLong(max: Long): TIO[Long] = PIO.pure(Math.min(_long, max))
}