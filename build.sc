import mill._, scalalib._, scalafmt._, mill.scalajslib._, mill.scalajslib.api._, publish._
import coursier.maven.MavenRepository
import $file.scripts.gitlab

// ####################################################################################################

trait Constants {
  val ScalaVersion = "2.13.10"

  val Version         = "0.3.0.1"
  val Organization    = "com.kelvaya"
  val AutoApiMappings = true

  val ScalaReflect = ivy"org.scala-lang:scala-reflect:${ScalaVersion}"

  val CatsEffect = ivy"org.typelevel::cats-effect::3.3.14"
  val Shapeless = ivy"com.chuusai::shapeless:2.3.10"

  val MUnit      = ivy"org.scalameta::munit::0.7.29"
  val ScalaCheck = ivy"org.scalacheck::scalacheck:1.17.0"
}

// ####################################################################################################

trait CommonModule extends ScalaModule with ScalafmtModule with Constants {
  def scalaVersion    = T(ScalaVersion)
  def version         = T(Version)
  // def ammoniteVersion = T("2.3.8")
  def organization    = T(Organization)
  def autoAPIMappings = T(AutoApiMappings)
  def scalacOptions   = T(
    Seq(
      "-deprecation",  // Emit warning and location for usages of deprecated APIs.
      "-encoding",
      "utf-8",         // Specify character encoding used by source files.
      "-explaintypes", // Explain type errors in more detail.
      "-language:higherKinds",
      "-feature",
      "-Xlint:_",
      "-Wunused:imports",
      "-unchecked"
    )
  )
}

trait CrossModule extends CommonModule {
  def sources =
    T.sources(
      millSourcePath / os.up / "shared" / "src",
      millSourcePath / "src"
    ) 
}

object jvm extends CrossModule with gitlab.GitlabPublishModule {
  object test extends Tests {
    def ivyDeps = Agg(MUnit, ScalaCheck)

    def testFramework = "munit.Framework"

    // from https://github.com/lihaoyi/mill/issues/344#issuecomment-445741323
    def testOne(args: String*) =
      T.command {
        super.runMain("org.scalatest.run", args: _*)
      }
  }

  def scalaDocOptions =
    super.scalaDocOptions.map {
      _ ++
        Seq(
          "-doc-root-content",
          "docs/root.html",
          "-doc-title",
          "Simple IO",
          "-doc-version",
          s"$Version"
        )
    }

  override def unmanagedClasspath =
    T(
      Agg(PathRef(millSourcePath / os.up / "local" / "cp"))
    )

  def ivyDeps = Agg(CatsEffect, Shapeless, ScalaReflect)

  def publishVersion = Version
  def artifactName = "wio"
  def gitlabPackageId = "26623584"
  def pomSettings = PomSettings(
    description = "Simple IO Effect",
    organization = "com.kelvaya",
    url = "https://gitlab.com/pennwarrior/wio",
    licenses = Seq(License.MIT),
    versionControl = VersionControl.gitlab("pennwarrior", "wio"),
    developers = Seq(
      Developer("pennwarrior", "Mike Intravaia","https://github.com/intro96git")
    )
  )
}

object wiotest extends CommonModule with gitlab.GitlabPublishModule {
  override def unmanagedClasspath =
    T(
      Agg(PathRef(millSourcePath / os.up / "local" / "cp"))
    )

  def ivyDeps = Agg(CatsEffect, Shapeless, ScalaReflect)

  def moduleDeps = Seq(jvm)

  def publishVersion = Version
  def gitlabPackageId = "26623584"  
  def artifactName = "wio-test"
  def pomSettings = PomSettings(
    description = "Simple IO Effect Test Suite",
    organization = "com.kelvaya",
    url = "https://gitlab.com/pennwarrior/wio",
    licenses = Seq(License.MIT),
    versionControl = VersionControl.gitlab("pennwarrior", "wio"),
    developers = Seq(
      Developer("pennwarrior", "Mike Intravaia","https://github.com/intro96git")
    )
  )
}
