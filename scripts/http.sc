import java.util.Base64

import scala.annotation.tailrec
import scala.concurrent.duration._

import mill.BuildInfo
import requests.BaseSession

class GitlabHttpApi(
  uri: String,
  user: String,
  pass: String,
  readTimeout: Int,
  connectTimeout: Int
) {
  val http = requests.Session(readTimeout = readTimeout, connectTimeout = connectTimeout, maxRedirects = 0, check = false)

  private val uploadTimeout = 5.minutes.toMillis.toInt

  def upload(uri: String, data: Array[Byte]): requests.Response = {
    http.put(
      uri,
      readTimeout = uploadTimeout,
      headers = Seq(
        "Content-Type" -> "application/binary",
        user -> pass
      ),
      data = data
    )
  }

  @tailrec
  private def withRetry(request: => requests.Response,
                        retries: Int = 10,
                        initialTimeout: FiniteDuration = 100.millis): requests.Response = {
    val resp = request
    if (resp.is5xx && retries > 0) {
      Thread.sleep(initialTimeout.toMillis)
      withRetry(request, retries - 1, initialTimeout * 2)
    } else {
      resp
    }
  }
}
