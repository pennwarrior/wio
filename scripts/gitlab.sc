import java.math.BigInteger
import java.security.MessageDigest

import mill.define.{Command, ExternalModule, Target, Task}
import mill.api.{Logger, PathRef}
import mill.main.Tasks
import mill.scalalib.publish.{Artifact}
import mill.scalalib.PublishModule

import os.Shellable

import $file.http

/**
  * Configuration necessary for publishing a Scala module to Maven Central or similar
  */
trait GitlabPublishModule extends PublishModule { outer =>
  import mill.scalalib.publish._

  def gitlabPackageId: String

  def gitlabUri: String = s"https://gitlab.com/api/v4/projects/${gitlabPackageId}/packages/maven"

  /**
    * Publish all given artifacts to Gitlab.
    * @param gpgArgs GPG arguments. Defaults to `--batch --yes -a -b`.
    *                 Specifying this will override/remove the defaults. Add the default args to your args to keep them.
    */
  def publishGitlab(gitlabUser: String,
              gitlabPass: String,
              signed: Boolean = true,
              gpgArgs: Seq[String] = PublishModule.defaultGpgArgs,
              release: Boolean = false,
              readTimeout: Int = 60000,
              connectTimeout: Int = 5000,
              awaitTimeout: Int = 120 * 1000): define.Command[Unit] = T.command {
    val PublishModule.PublishData(artifactInfo, artifacts) = publishArtifacts()
    new GitlabPublisher(
      gitlabUri,
      gitlabUser,
      gitlabPass,
      signed,
      gpgArgs.flatMap(_.split("[,]")),
      readTimeout,
      connectTimeout,
      T.log,
      awaitTimeout
    ).publish(artifacts.map{case (a, b) => (a.path, b)}, artifactInfo, release)
  }
}

class GitlabPublisher(uri: String,
                        user: String,
                        pass: String,
                        signed: Boolean,
                        gpgArgs: Seq[String],
                        readTimeout: Int,
                        connectTimeout: Int,
                        log: Logger,
                        awaitTimeout: Int) {

  private val api = new http.GitlabHttpApi(uri, user, pass, readTimeout = readTimeout, connectTimeout = connectTimeout)

  def publish(fileMapping: Seq[(os.Path, String)], artifact: Artifact, release: Boolean): Unit = {
    publishAll(release, fileMapping -> artifact)
  }

  def publishAll(release: Boolean, artifacts: (Seq[(os.Path, String)], Artifact)*): Unit = {
    val mappings = for ((fileMapping0, artifact) <- artifacts) yield {
      val publishPath = Seq(
        artifact.group.replace(".", "/"),
        artifact.id,
        artifact.version
      ).mkString("/")
      val fileMapping = fileMapping0.map { case (file, name) => (file, publishPath + "/" + name) }

      val signedArtifacts = if (signed) fileMapping.map {
        case (file, name) => gpgSigned(file, gpgArgs) -> s"$name.asc"
      } else Seq()

      artifact -> (fileMapping ++ signedArtifacts).flatMap {
        case (file, name) =>
          val content = os.read.bytes(file)

          Seq(
            name -> content,
            (name + ".md5") -> md5hex(content),
            (name + ".sha1") -> sha1hex(content)
          )
      }
    }

    val (snapshots, releases) = mappings.partition(_._1.isSnapshot)
    if (snapshots.nonEmpty) {
      publishSnapshot(snapshots.flatMap(_._2), snapshots.map(_._1))
    }
    val releaseGroups = releases.groupBy(_._1.group)
    for ((group, groupReleases) <- releaseGroups) {
      publishReleaseNonstaging(groupReleases.flatMap(_._2), releases.map(_._1))
    }
  }

  private def publishSnapshot(payloads: Seq[(String, Array[Byte])],
                              artifacts: Seq[Artifact]): Unit = {
    publishToUri(payloads, artifacts, uri)
  }

  private def publishToUri(payloads: Seq[(String, Array[Byte])],
                           artifacts: Seq[Artifact],
                           uri: String): Unit = {
    val publishResults = payloads.map {
      case (fileName, data) =>
        log.info(s"Uploading $fileName to $uri/$fileName")
        api.upload(s"$uri/$fileName", data)
    }
    reportPublishResults(publishResults, artifacts)
  }

  private def publishReleaseNonstaging(payloads: Seq[(String, Array[Byte])],
                                       artifacts: Seq[Artifact]): Unit = {
    publishToUri(payloads, artifacts, uri)
  }


  private def reportPublishResults(publishResults: Seq[requests.Response],
                                   artifacts: Seq[Artifact]): Unit = {
    if (publishResults.forall(_.is2xx)) {
      log.info(s"Published ${artifacts.map(_.id).mkString(", ")} to Gitlab")
    } else {
      val errors = publishResults.filterNot(_.is2xx).map { response =>
        s"Code: ${response.statusCode}, message: ${response.text()}"
      }
      throw new RuntimeException(
        s"Failed to publish ${artifacts.map(_.id).mkString(", ")} to Gitlab. Errors: \n${errors.mkString("\n")}"
      )
    }
  }

  // http://central.sonatype.org/pages/working-with-pgp-signatures.html#signing-a-file
  private def gpgSigned(file: os.Path, args: Seq[String]): os.Path = {
    val fileName = file.toString
    val command = "gpg" +: args :+ fileName

    os.proc(command.map(v => v: Shellable))
      .call(stdin = os.Inherit, stdout = os.Inherit, stderr = os.Inherit)
    os.Path(fileName + ".asc")
  }

  private def md5hex(bytes: Array[Byte]): Array[Byte] =
    hexArray(md5.digest(bytes)).getBytes

  private def sha1hex(bytes: Array[Byte]): Array[Byte] =
    hexArray(sha1.digest(bytes)).getBytes

  private def md5 = MessageDigest.getInstance("md5")

  private def sha1 = MessageDigest.getInstance("sha1")

  private def hexArray(arr: Array[Byte]) =
    String.format("%0" + (arr.length << 1) + "x", new BigInteger(1, arr))

}
